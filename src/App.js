import React, { Component } from 'react';
import './App.less';
import Header from './components/Header';
import Description from './components/Description';
import Education from './components/Education';

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = { name: '', age: '', description: '' };
    fetch('http://localhost:3000/person')
      .then(response => response.json())
      .then(result => {
        // eslint-disable-next-line no-console
        console.log(result);
        this.setState({
          name: result.name,
          age: result.age,
          description: result.description,
          educations: result.educations
        });
      });
  }

  render() {
    return (
      <main className="app">
        <Header name={this.state.name} age={this.state.age} />
        <hr />
        <Description description={this.state.description} />
        <Education educations={this.state.educations} />
      </main>
    );
  }
}

export default App;
