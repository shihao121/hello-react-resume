import React from 'react';

const Education = props => {
  return (
    <article>
      <h3>EDUCATION</h3>
      <section className={'education'}>
        <ul className={'ul-education'}>
          {props.educations === undefined
            ? ''
            : props.educations.map(education => {
                return (
                  <div key={education.year + education.title}>
                    <li key={education.year}>{education.year}</li>
                    <li key={education.title}>{education.title}</li>
                    <li key={education.description}>{education.description}</li>
                  </div>
                );
              })}
        </ul>
      </section>
    </article>
  );
};

export default Education;
