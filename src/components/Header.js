import React from 'react';

const Header = props => {
  return (
    <header>
      <img src={require('../assets/avatar.jpg')} alt={'photo'} />
      <h1>Hello,</h1>
      <h2 className={'introduce'}>
        my name is {props.name}
        {props.age} yo and this is my resume/cv
      </h2>
    </header>
  );
};

export default Header;
