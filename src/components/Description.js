import React from 'react';

const Description = props => {
  return (
    <section>
      <h3>DESCRIPTION</h3>
      {/* eslint-disable-next-line react/prop-types */}
      <p className="description">{props.description}</p>
    </section>
  );
};

export default Description;
